<?php
require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

    try {
        $conexion = new PDO($dsn, $usuario, $contraseña);

        $new_user = array(
            "apellido_paterno" => $_POST['apellido_paterno'],
            "apellido_materno"  => $_POST['apellido_materno'],
            "nombres"  => $_POST['nombres'],
            "email"     => $_POST['email'],
            "edad"       => $_POST['edad'],
            "procedencia"  => $_POST['procedencia']
        );

        $sql = sprintf(
            "INSERT INTO %s (%s) values (%s)",
            "usuarios",
            implode(", ", array_keys($new_user)),
            ":" . implode(", :", array_keys($new_user))
        );

        $statement = $conexion->prepare($sql);
        $statement->execute($new_user);
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>

<?php require "templates/header.php"; ?>

<!-- Agrega el estilo CSS -->
<style>
    form {
        margin-top: 20px;
    }

    label {
        display: block;
        margin-bottom: 5px;
        font-weight: bold;
    }

    input[type="text"] {
        width: 300px;
        padding: 5px;
        margin-bottom: 10px;
    }

    .button {
        background-color: #4caf50;
        color: white;
        padding: 10px 20px;
        border: none;
        cursor: pointer;
        border-radius: 4px;
    }

    .button-secondary {
        background-color: #008CBA;
    }

    .blockquote {
        background-color: #f9f9f9;
        border-left: 10px solid #ccc;
        margin: 1.5em 0;
        padding: 0.5em 10px;
    }
</style>

<?php if (isset($_POST['submit']) && $statement) : ?>
    <blockquote><?php echo escape($_POST['apellido_paterno']); ?> Agregado exitosamente.</blockquote>
<?php endif; ?>

<h2>Agregar Usuario</h2>
<form method="post">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <label for="apellido_paterno">Apellido Paterno</label>
    <input type="text" name="apellido_paterno" id="apellido_paterno">
    <label for="apellido_materno">Apellido Materno</label>
    <input type="text" name="apellido_materno" id="apellido_materno">
    <label for="nombres">Nombres</label>
    <input type="text" name="nombres" id="nombres">
    <label for="email">Email</label>
    <input type="text" name="email" id="email">
    <label for="edad">Edad</label>
    <input type="text" name="edad" id="edad">
    <label for="procedencia">Procedencia</label>
    <input type="text" name="procedencia" id="procedencia">
    <input type="submit" name="submit" value="Submit" class="button">
</form>

<a href="index.php" class="button button-secondary">Regresar</