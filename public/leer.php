<?php
require "../conexion.php";
require "../common.php";

if (isset($_POST['submit'])) {
    if (!hash_equals($_SESSION['csrf'], $_POST['csrf'])) die();

    try {
        $connection = new PDO($dsn, $usuario, $contraseña);

        $sql = "SELECT * 
            FROM usuarios
            WHERE procedencia = :procedencia";

        $procedencia = $_POST['procedencia'];
        $statement = $connection->prepare($sql);
        $statement->bindParam(':procedencia', $procedencia, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();
    } catch (PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>

<?php require "templates/header.php"; ?>

<!-- Agrega el estilo CSS -->
<link rel="stylesheet" href="estilo.css">

<h2>Encontrar usuario basado en procedencia</h2>

<form method="post" class="filter-menu">
    <input name="csrf" type="hidden" value="<?php echo escape($_SESSION['csrf']); ?>">
    <label for="procedencia">Procedencia</label>
    <input type="text" id="procedencia" name="procedencia">
    <input type="submit" name="submit" value="Ver resultados" class="button">
</form>

<?php
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) { ?>
        <h2>Results</h2>

        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Nonbres</th>
                    <th>Email</th>
                    <th>Edad</th>
                    <th>Procedencia</th>
                    <th>Fecha</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($result as $row) : ?>
                    <tr>
                        <td><?php echo escape($row["id"]); ?></td>
                        <td><?php echo escape($row["apellido_paterno"]); ?></td>
                        <td><?php echo escape($row["apellido_materno"]); ?></td>
                        <td><?php echo escape($row["nombres"]); ?></td>
                        <td><?php echo escape($row["email"]); ?></td>
                        <td><?php echo escape($row["edad"]); ?></td>
                        <td><?php echo escape($row["procedencia"]); ?></td>
                        <td><?php echo escape($row["fecha"]); ?> </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php } else { ?>
        <blockquote>No results found for <?php echo escape($_POST['procedencia']); ?>.</blockquote>

<?php }
} ?>
<a href="index.php">Regresar al inicio</a>

<?php require "templates/footer.php"; ?>